package scmuteam.scmu;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import scmuteam.scmu.Server.*;


public class InitPage extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener{

    private BottomNavigationView navigationView;
    ArrayList<Container> containerList;
    private ListAdapter containersAdapter;
    private ListView containers;
    public static volatile String answerServer;
    public static volatile boolean threadOn;
    private Thread myThread;
    private Fragment notiFragment;
    private Fragment mapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.initpage);


        navigationView = (BottomNavigationView) findViewById(R.id.navigationView);
        navigationView.setOnNavigationItemSelectedListener(this);
        navigationView.setSelectedItemId(R.id.navigation_open);


        containers = (ListView) findViewById(R.id.list_notifi);
        containerList = new ArrayList<>();

        threadOn = true;

        answerServer = "";
        myThread = new Thread(new ServerThrea());
        myThread.start();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        notiFragment = new Notifications();
        mapFragment = Map.newInstance();
        Bundle bundle = new Bundle();
        //put your ArrayList data in bundle
        bundle.putSerializable("bundle_key",containerList);
        notiFragment.setArguments(bundle);
        mapFragment.setArguments(bundle);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navigation_noti: {
                if(containerList != null){
                    getSupportActionBar().setTitle("Containers");
                    openFragment(notiFragment);
                }

                break;
            }
            case R.id.navigation_open: {
                getSupportActionBar().setTitle("Manual Open/Close");
                Fragment manualFragment = ManualOpen.newInstance();
                openFragment(manualFragment);
                break;
            }
            case R.id.navigation_map: {
                getSupportActionBar().setTitle("Map");
                openFragment(mapFragment);
                break;
            }
        }
        return true;
    }

    private void openFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }


    class ServerThrea implements Runnable {

        Socket s;
        ServerSocket ss;
        InputStreamReader isr;
        BufferedReader br;

        Handler h = new Handler(); //print message as a popup


        @Override
        public void run() {

            // MainActivity.answerServer = "true";
            s = null;
            try {
                ss = new ServerSocket(); // <-- create an unbound socket first
                ss.setReuseAddress(true);
                ss.bind(new InetSocketAddress(7777)); // <-- now bind it
            } catch (UnknownHostException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            while (threadOn) {
                try {
                    s = ss.accept();
                    isr = new InputStreamReader(s.getInputStream());
                    br = new BufferedReader(isr);
                    answerServer = br.readLine();
                    String [] separar = answerServer.split(" ");
                    containerList.add(new Container(separar[0], separar[1], separar[2], separar[3]));

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } catch (UnknownHostException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }


}
