package scmuteam.scmu;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class Map extends Fragment {


    private SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private float markerColor;

    ArrayList<Container> containerList;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.map, container, false);

        Bundle args = getArguments();
        containerList = (ArrayList<Container>) args.getSerializable("bundle_key");

        if (mapFragment == null) {
            mapFragment = SupportMapFragment.newInstance();
            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {

                    mMap  = googleMap;
                    mMap.setMinZoomPreference(6.7f);
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Almada, 12f));

                    for(int i = 0; i < containerList.size(); i++){
                        makeMarker(containerList.get(i));
                    }
                }
            });
        }

        // R.id.mapi is a FrameLayout, not a Fragment
        getChildFragmentManager().beginTransaction().replace(R.id.mapi, mapFragment).commit();



        return view;
    }

    public static LatLng Almada = new LatLng(38.676341, -9.165174);
    public static LatLng FCT = new LatLng(38.661614, -9.205732);

    public static LatLngBounds PORTUGAL = new LatLngBounds(
            new LatLng(36.746936, -10.684079), new LatLng(42.259166, -5.981752));

    public static Map newInstance() {
        return new Map();
    }

    private void makeMarker(Container container) {

        Double lat = Double.parseDouble(container.getLat());
        Double lng = Double.parseDouble(container.getLng());


        Marker m = mMap.addMarker(new MarkerOptions()
                .title(container.getName())
                .position(new LatLng(lng, lat))
                .draggable(false)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
        );
        m.setTag(container);

    }


}

