package scmuteam.scmu.Server;

import android.os.AsyncTask;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public class SendRequest extends AsyncTask<Void, Void, Void> {

    private static Socket s;
    private static PrintWriter printWriter;

    String message = "";
    private static String ip = "192.168.1.11";
    //private static String ip = "192.168.43.222";


    public SendRequest (String message) {
        this.message = message;
    }

    @Override
    protected Void doInBackground(Void... params) {

        s=null;
        try {
            s = new Socket(ip, 5000); //connect to the socket at port 5000
            s.setReuseAddress(true);
            printWriter = new PrintWriter(s.getOutputStream()); //set the output stream
            printWriter.write(message);     //send the message through the socket
            printWriter.flush();
            printWriter.close();
            s.close();



        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
