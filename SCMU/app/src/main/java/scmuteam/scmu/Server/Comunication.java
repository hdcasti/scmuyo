package scmuteam.scmu.Server;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import scmuteam.scmu.R;

public class Comunication extends AppCompatActivity {

    EditText e1;
    private static Socket s;
    private static PrintWriter printWriter;

    String message = "";
    private static String ip = "192.168.1.9";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Thread myThread = new Thread(new MyServerThread());
        myThread.start();

    }

    public void send_text(View v){
        message = e1.getText().toString();

        myTask mt = new myTask();
        mt.execute();

        Toast.makeText(getApplicationContext(), "Data sent", Toast.LENGTH_LONG).show();
    }

    class MyServerThread implements Runnable{

        Socket s;
        ServerSocket ss;
        InputStreamReader isr;
        BufferedReader br;
        String message = "";

        Handler h = new Handler(); //print message as a popup

        @Override
        public void run() {

            try {
                ss = new ServerSocket(7777);
                while(true){
                    s = ss.accept();
                    isr = new InputStreamReader(s.getInputStream());
                    br = new BufferedReader(isr);
                    message = br.readLine();

                    h.post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        }
                    });


                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    class myTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            try{
                s = new Socket(ip, 5000); //connect to the socket at port 5000
                printWriter = new PrintWriter(s.getOutputStream()); //set the output stream
                printWriter.write(message);     //send the message through the socket
                printWriter.flush();
                printWriter.close();
                s.close();

            }catch(IOException e){
                e.printStackTrace();
            }
            return null;
        }
    }
}