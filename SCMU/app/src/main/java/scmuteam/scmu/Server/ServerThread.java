package scmuteam.scmu.Server;

import android.os.Handler;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

import scmuteam.scmu.InitPage;
import scmuteam.scmu.MainActivity;

public class ServerThread implements Runnable{

    // Socket s;
    // ServerSocket ss;
    private static Socket s;
    private static ServerSocket ss;
    public static volatile  boolean threadOn;
    InputStreamReader isr;
    BufferedReader br;

    Handler h = new Handler(); //print message as a popup

    public static synchronized Socket getSocket(){
        return s;
    }

    public static synchronized void setSocket(Socket socket){
        ServerThread.s = socket;
    }

    public static synchronized ServerSocket getServerSocket(){
        return ss;
    }

    public static synchronized void setServerSocket(ServerSocket socket){
        ServerThread.ss = socket;
    }



    @Override
    public void run() {

        // MainActivity.answerServer = "true";

        try {
            ss = new ServerSocket(); // <-- create an unbound socket first
            ss.setReuseAddress(true);
            ss.bind(new InetSocketAddress(7777)); // <-- now bind it
            while(threadOn){

                s = ss.accept();
                isr = new InputStreamReader(s.getInputStream());
                br = new BufferedReader(isr);

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}