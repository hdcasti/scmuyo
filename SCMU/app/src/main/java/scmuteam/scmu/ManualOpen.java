package scmuteam.scmu;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import scmuteam.scmu.Server.SendRequest;

public class ManualOpen extends Fragment {

    private CardView openCard, closeCard;
    private ImageView open;
    private ImageView openBW;
    private ImageView close;
    private ImageView closeBW;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.manual_open_close, container, false);

        openCard = view.findViewById(R.id.open_card);
        closeCard = view.findViewById(R.id.close_card);

        open = view.findViewById(R.id.open);
        openBW = view.findViewById(R.id.open_bw);
        close = view.findViewById(R.id.close);
        closeBW = view.findViewById(R.id.close_bw);

        openCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                open.setVisibility(View.VISIBLE);
                openBW.setVisibility(View.GONE);
                close.setVisibility(View.GONE);
                closeBW.setVisibility(View.VISIBLE);
                String message = "Abrir";
                SendRequest sr = new SendRequest(message);
                sr.execute();
            }
        });

        closeCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                open.setVisibility(View.GONE);
                openBW.setVisibility(View.VISIBLE);
                close.setVisibility(View.VISIBLE);
                closeBW.setVisibility(View.GONE);
                String message = "Fechar";
                SendRequest sr = new SendRequest(message);
                sr.execute();
            }
        });



        return view;
    }
    public static ManualOpen newInstance() {
        return new ManualOpen();
    }
}

