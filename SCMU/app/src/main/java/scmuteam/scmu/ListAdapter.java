package scmuteam.scmu;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class ListAdapter extends ArrayAdapter<Container> {

    Context context;
    ArrayList<Container> listArray;

    public ListAdapter(Context context, int resource, ArrayList<Container> objects) {
        super(context, resource, objects);
        this.context = context;
        listArray = objects;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = ((Activity) getContext()).getLayoutInflater().inflate(R.layout.single_item, parent, false);
        }

        TextView name = convertView.findViewById(R.id.identificador);
        TextView location = convertView.findViewById(R.id.localiza);
        TextView peso = convertView.findViewById(R.id.estado);

        final Container cont = getItem(position);

        name.setText(cont.getName());
        location.setText(cont.getLocation());

        if(cont.getName().equals("Balança")){
            peso.setText("80%");
            peso.setTextColor(getContext().getResources().getColor(R.color.red));
        } else {
            peso.setText("20%");
        }

        return convertView;
    }


}
