package scmuteam.scmu;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import java.util.ArrayList;

public class Notifications extends Fragment {

    private ListAdapter containersAdapter;
    private ListView containers;
    ArrayList<Container> containerList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.notifi, container, false);

        containers = (ListView) view.findViewById(R.id.list_notifi);

        Bundle args = getArguments();
        containerList = (ArrayList<Container>) args.getSerializable("bundle_key");
        containersAdapter = new ListAdapter(getActivity(), R.layout.notifi, containerList);
        containers.setAdapter(containersAdapter);

        return view;
    }

    public static Notifications newInstance() {
        return new Notifications();
    }

}

