package scmuteam.scmu;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import scmuteam.scmu.Server.SendRequest;

public class MainActivity extends AppCompatActivity {

    private ProgressBar progressBar;
    CountDownTimer mCountDownTimer;
    int i = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar = findViewById(R.id.progressBar);

        mCountDownTimer=new CountDownTimer(5000,1000) {

            @Override
            public void onTick(long millisUntilFinished) {


            }

            @Override
            public void onFinish() {
                //Do what you want
                String message = "a";
                SendRequest sr = new SendRequest(message);
                sr.execute();
                Intent intent  = new Intent(MainActivity.this, InitPage.class);
                startActivity(intent);
                finish();
            }
        };
        mCountDownTimer.start();

    }
}
