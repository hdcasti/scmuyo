package scmuteam.scmu;

import java.io.Serializable;

public class Container implements Serializable {

    String name;
    int peso;
    int volume;
    private String lng;
    private String lat;
    private String location;

    public Container(String name, String lng, String lat, String location){
        this.name = name;
        this.lng = lng;
        this.lat = lat;
        this.location = location;
    }

    public String getLocation() {
        return location;
    }

    public String getName() {
        return name;
    }

    public String getLng(){
        return lng;
    }

    public String getLat() {
        return lat;
    }

}
