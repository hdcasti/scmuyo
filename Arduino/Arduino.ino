#include <math.h>
#include <SoftwareSerial.h>
#include <HX711.h>

SoftwareSerial mySerial(2, 3); // RX, TX

int Redled = 9; // the pin the RED LED is connected to
int Greenled = 8; // the pin the Green LED is connected to

#define DOUT  6
#define CLK  7

HX711 scale;
#define calibration_factor -7050.0

const int trigPin = 11;
const int echoPin = 12;
// defines variables
long duration;
int distance;

int sensorpin = 0;   // analog pin used to connect the sharp sensor
int val = 0;         // variable to store the values from sensor(initially zero)


void setup() {
Serial.begin(9600); // Starts the serial communication
mySerial.begin(9600);

pinMode(Redled, OUTPUT); // Declare the LED as an output
pinMode(Greenled, OUTPUT); // Declare the LED as an output

scale.begin(DOUT, CLK);
scale.set_scale(calibration_factor); //This value is obtained by using the SparkFun_HX711_Calibration sketch
scale.tare(); //Assuming there is no weight on the scale at start up, reset the scale to 0
  
pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
pinMode(echoPin, INPUT); // Sets the echoPin as an Input


}

void loop() {
  
  delay(400);

  val = analogRead(sensorpin);
  Serial.print("Infrared: ");
  Serial.println(val); 
  //mySerial.write("Infrared: ");// prints the value of the sensor to the serial monitor
 // mySerial.write(val);
  String buf;
  buf += F("Infrared_");
  buf += String(val);
  mySerial.print(buf);
  delay(400);
  
  Serial.print("Balanca: ");
  float lbs = scale.get_units();
  if (lbs < 0) {
    lbs = 0.00;
  }
  float kg = lbs * 0.45359;
  Serial.println(kg, 1); //scale.get_units() returns a float
  //mySerial.write(kg, 1);
  String buf2;
  buf2 += F("Balanca_");
  buf2 += String(kg);
  mySerial.print(buf2);
  delay(400);
  
// Clears the trigPin
digitalWrite(trigPin, LOW);
delayMicroseconds(50000);
// Sets the trigPin on HIGH state for 10 micro seconds
digitalWrite(trigPin, HIGH);
delayMicroseconds(100);
digitalWrite(trigPin, LOW);
// Reads the echoPin, returns the sound wave travel time in microseconds
duration = pulseIn(echoPin, HIGH);
// Calculating the distance
distance= duration*0.034/2;


if(distance < 4){
  digitalWrite(Greenled, HIGH);// Turn the LED on
  digitalWrite(Redled, LOW);
} else{
  digitalWrite(Redled, HIGH);// Turn the LED on
  digitalWrite(Greenled, LOW);
}
// Prints the distance on the Serial Monitor
delay(1000);
Serial.print("Distance: ");
Serial.println(distance);

//mySerial.print("HI!");
/*String buf;
  buf += F("Sonar_");
  buf += String(distance);
  mySerial.print(buf);
  String rec = mySerial.readString();*/
  //Serial.println(rec);

delay(1000);

  
}
