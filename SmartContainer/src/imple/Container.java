package imple;

public class Container {
	
	private String containerIdentifier;
	private int WeigthBalance1;
	private int WeigthBalance2;
	private int infrared;
	private int totalWeigth;
	private String lat;
	private String lng;
	private String location;
	
	
	public Container( String containerName, int WeigthBalance1, int WeigthBalance2, String lat, String lng, String location) {
		this.containerIdentifier = containerName;
		this.WeigthBalance1 = WeigthBalance1;
		this.WeigthBalance2 = WeigthBalance2;
		this.lat = lat;
		this.lng = lng;
		this.location = location;
	}
	
	public Container(String containerName, int infrared,String lat, String lng, String location) {
		this.containerIdentifier = containerName;
		this.infrared = infrared;
		this.lat = lat;
		this.lng = lng;
		this.location = location;
	}
	
	public String getLat() {
		return lat;
	}
	
	public String getLocation() {
		return location;
	}
	
	public String getLng() {
		return lng;
	}
	
	public String getContainerName() {
		return containerIdentifier;
	}
	
	public int getWeigthBalance1() {
		return WeigthBalance1;
	}
	
	public int getWeigthBalance2() {
		return WeigthBalance2;
	}
	
	public int getInfrared() {
		return infrared;
	}
	
	public void setTotalWeigth() {
		totalWeigth = (getWeigthBalance1() + getWeigthBalance2()) / 2;
	}
	
	public int getTotalWeigth() {
		return totalWeigth;
	}
	
	public void addSensorMeasures(String SensorName, int value) {
		if (SensorName.equals("WeigthBalance1")) {
			WeigthBalance1 = value;
		} else if (SensorName.equals("WeigthBalance1")) {
			WeigthBalance2 = value;
		} else if (SensorName.equals("Infrared")) {
			infrared = value;
		} 
	}

}
