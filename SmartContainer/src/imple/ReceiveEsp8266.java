package imple;

import java.io.IOException;
import java.net.URI;
import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import api.ServerInterface;



public class ReceiveEsp8266 implements ServerInterface, Runnable {

	static String URI_BASE;
	static int serverPort = 9999;

	public static volatile double WeigthBalance1;
	public static volatile double WeigthBalance2;
	public static volatile int Infrared;
	public static volatile double Sonar;

	@Override
	public String storeSensorValues(String value) throws IOException {

		String[] aux = value.split("_");
		
		System.out.println(value);

		if (aux[0].equals("WeigthBalance1")) {
			WeigthBalance1 = Double.parseDouble(aux[1]);

		} else if (aux[0].equals("WeigthBalance2")) {
			WeigthBalance2 = Double.parseDouble(aux[1]);

		} else if (aux[0].equals("Infrared")) {
			Infrared = Integer.parseInt(aux[1]);
			
		} else if (aux[0].equals("Sonar")) {
			Sonar = Double.parseDouble(aux[1]);
		}
		
		return value;

	}

	@Override
	public void run() {

		URI_BASE = "http://" + Library.hostAddress() + ":" + serverPort + "/";

		System.out.println();
		System.out.println("--------------------------------------------------------------");
		System.out.println("Server: Receive data from ESP8266");
		System.out.println(URI_BASE);
		System.out.println("--------------------------------------------------------------");
		System.out.println();

		ResourceConfig config = new ResourceConfig();
		config.register(new ReceiveEsp8266());

		JdkHttpServerFactory.createHttpServer(URI.create(URI_BASE.toString()), config);
		
		
;
	}
}
