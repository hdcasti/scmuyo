package imple;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Main {

	private static ServerSocket ss;
	private static Socket s;
	private static BufferedReader br;
	private static InputStreamReader isr;

	private static String message = "";
	private static int portToReceive = 5000;
	private static int portToSendToAndroid = 7777;
	
	public static List<Container> listOfContainers;


	static final int arduinoId = 1;

	
	public Main() {
		listOfContainers = new ArrayList<Container>();
		
	}
	
	public static void main(String[] args) {
		new Main();
		
	
		Thread handleSensors = new Thread(new ReceiveEsp8266());
		handleSensors.start();
		
		Container container1 = new Container("Balança", 0 ,0, "38.676341", "-9.165174", "Almada");
		Container container2 = new Container("InfraRed",0, "38.661614", "-9.205732", "FCT-UNL");
		
		listOfContainers.add(container1);
		listOfContainers.add(container2);
		
		
		
		try {
			ss = new ServerSocket(portToReceive);			
			while (true) {
				s = ss.accept();
				isr = new InputStreamReader(s.getInputStream()); // to receive data
				br = new BufferedReader(isr);
				message = br.readLine();

				System.out.println();
				System.out.println("--------------------------------------------------------------");
				System.out.println("Message Received from Android: " + message);
				System.out.println();

				//String[] aux = message.split("_");
				
				System.out.println("  " + ReceiveEsp8266.URI_BASE);
				if(message.equals("a")) {
				for(int i = 0; i< listOfContainers.size(); i++) {
					new SendToAndroid().send(listOfContainers.get(i).getContainerName() + " " + listOfContainers.get(i).getLat() + " "
							+ listOfContainers.get(i).getLng() + " " + listOfContainers.get(i).getLocation(), 7777);
					
					}
				} else {
					;
				}
				/*isr.close();
				br.close();
				ss.close();
				s.close();*/
			}
		} catch(Exception e) {
			
		}
		
	}

}
