package imple;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class Library {
	
	public static String hostAddress() {
		try {
			return InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			return "?.?.?.?";
		}
	}

	static java.util.Random rnd = new java.util.Random();

	public static String key64() {
		return Long.toString(System.nanoTime() >>> 1, 32);
	}	

}
