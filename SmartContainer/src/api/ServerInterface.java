package api;

import java.io.IOException;
import javax.ws.rs.*;
import javax.ws.rs.core.*;


@Path( ServerInterface.PATH )
public interface ServerInterface {
	
	static final String PATH = "/datanode";
    
    @GET
    @Path("/{value}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    String storeSensorValues(@PathParam("value") String value) throws IOException;
}
